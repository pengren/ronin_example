# RoNIN on the iPhone

## ☁️ Introduction

This is an ios app that can be used to illustrate the RoNIN.

## ⚡️ Quickstart

* Download the repo:

  ```
  ​```
  git clone https://pengren@bitbucket.org/pengren/ronin_example.git
  ​```
  ```

* Use Xcode to open the **CoreMotionExample.xcworkspace** instead of the **CoreMotionExample.xcodeproj**

* Take a look at the CoreMotionExample -> TARGETS -> CoreMotionExample->Signinng & Capabilities, the Team should be changed before we run the app:

![image1](images/1.png)

* Connect the iPhone to your mac, then select your phone here:

![image2](images/2.png)

* Click the triangle button on the top left to build and then run the current scheme:

![image3](images/3.png)

* If the process fail, we need to verify the developer app certificate:

![image4](images/4.png)

* Open your iPhone, select Settings -> General -> Device Management -> Apple Development:  your email address , and select **"Trust "Apple Development: your email address""**

* Click the triangle button on the top left again. The app should work this time. If it doesn't, please let me know. Thank you!

