//
//  mkf_util_tester.swift
//  mkf_util_tester
//
//  Created by renpeng on 11/29/19.
//  Copyright © 2019 Peng Ren. All rights reserved.
//

import XCTest
import AVFoundation
@testable import CoreMotionExample

class mkf_util_tester: XCTestCase {
    let mkf_util = MKF_util()
    
    func test_Choice_from_prob() {
        let orientation_candidate_list = [0.0, Double.pi/4, -Double.pi/4, Double.pi/2, -Double.pi/2, 3*Double.pi/4, -3*Double.pi/4, Double.pi]
        let prob_list = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 22.0]
        for _ in 0..<10{
            let res = self.mkf_util.Choice_from_prob(orientation_candidate_list, prob_list) as! Double
            // print("sampled_res: \(res)")
            XCTAssertEqual(res, Double.pi)
        }
    }
    
    func test_obtain_wrapped_distance() {
        let test_input = [[3.15, -3.12], [12.5, 3.13], [9.0, 1.0], [-3.15, -2.4], [3.15, -1.4], [3.15, 3.13], [3.15, -3.13], [-3.15, 3.13], [-3.15, -3.13]]
        var res = [Double]()
        for test_case in test_input{
            res.append(self.mkf_util.obtain_wrapped_distance(test_case[0], test_case[1]))
        }
        let correct_result = [0.01318530717958577, -3.086814692820414, -1.7168146928204138, 0.75, 1.7331853071795855, -0.019999999999999574, 0.0031853071795859833, -0.0031853071795859833, 0.020000000000000462]
        XCTAssertEqual(res, correct_result)
    }
    
    func test_wrap_azimuth_data() {
        let test_input = [3.15, -3.15, 2.5, 3.13, 9.0, -7.4]
        var res = [Double]()
        for test_case in test_input{
            res.append(self.mkf_util.wrap_azimuth_data(test_case))
        }
        let correct_result = [-3.133185307179586, 3.133185307179586, 2.5, 3.13, 2.7168146928204138, -1.1168146928204141]
        XCTAssertEqual(res, correct_result)
        
    }
    
    func test_obtain_real_angle_change() {
        let test_input = [[3.15, -3.15], [2.5, 3.13], [9.0, 27.0], [-3.15, -7.4], [3.15, -7.4], [3.13, 3.15], [3.13, -3.15], [-3.13, 3.15], [-3.13, -3.15]]
        var res = [Double]()
        for test_case in test_input{
            res.append(self.mkf_util.obtain_real_angle_change(test_case[0], test_case[1]))
            res.append(self.mkf_util.obtain_real_angle_change(test_case[1], test_case[0]))
        }
        let correct_result = [-0.01681469282041359, 0.01681469282041448, 0.6299999999999999, -0.6299999999999999, -0.8495559215387587, 0.8495559215387587, 2.0331853071795862, -2.0331853071795862, 2.0163706143591718, -2.0163706143591718, 0.020000000000000018, -0.020000000000000018, 0.0031853071795868715, -0.0031853071795868715, -0.0031853071795868715, 0.0031853071795868715, -0.020000000000000018, 0.020000000000000018]
        XCTAssertEqual(res, correct_result)
    }
    
    func test_normpdf() {
        let test_input = [[5.2, 1.2, 0.5], [1.3, 1.2, 0.9], [2.3, -1.2, 10.0], [-0.3, -0.2, 1.8]]
        var res = [Double]()
        for test_case in test_input{
            res.append(self.mkf_util.normpdf(test_case[0], test_case[1], test_case[2]))
        }
        let correct_result = [1.0104542167073785e-14, 0.44054139861676433, 0.03752403469169379, 0.221292835294303]
        XCTAssertEqual(res, correct_result)
    }
    
    func test_radius_to_string() {
        let test_input = [3.14, -3.14, 3.2, -3.3, -2.9, 2.6, -2.1, 1.7, -1.2, 0.9, -0.6, 0.3, -0.1]
        var res = [String]()
        for test_case in test_input{
            res.append(self.mkf_util.radius_to_string(test_case))
        }
        let correct_result = ["135", "-135", "180", "180", "-135", "135", "-90", "90", "-45", "45", "0", "0", "0"]
        XCTAssertEqual(res, correct_result)
    }
    
    func test_single_drift_update_limiter() {
        let test_input = [0.001, 0.02, 0.3, 4, 50, 600, 7000, -0.001, -0.02, -0.3, -4, -50, -600, -7000]
        var res = [Double]()
        for test_case in test_input{
            res.append(self.mkf_util.single_drift_update_limiter(test_case))
        }
        let correct_result = [0.00016826132809657411, 0.0004427521454014444, 0.000999999997789396, 0.001, 0.001, 0.001, 0.001, -0.00016826132809657411, -0.0004427521454014444, -0.000999999997789396, -0.001, -0.001, -0.001, -0.001]
        XCTAssertEqual(res, correct_result)
    }
    
    func test_round_to_nearest_orientation() {
        let test_input = [3.14, -3.14, 3.2, -3.3, -2.9, 2.6, -2.1, 1.7, -1.2, 0.9, -0.6, 0.3, -0.1]
        var res = [Double]()
        for test_case in test_input{
            res.append(self.mkf_util.round_to_nearest_orientation(test_case))
        }
        let correct_result = [3.141592653589793, -3.141592653589793, 3.141592653589793, -3.141592653589793, -3.141592653589793, 2.356194490192345, -2.356194490192345, 1.5707963267948966, -1.5707963267948966, 0.7853981633974483, -0.7853981633974483, 0.0, -0.0]
        XCTAssertEqual(res, correct_result)
    }
    
    func test_Multiple_Choices_from_prob() {
        var KL_list = [stand_alone_kalman_filter]()
        let list_length = 5
        for KL_index in 0..<8{
            KL_list.append(stand_alone_kalman_filter(KL_index))
        }
        let prob_list = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 22.0]
        var res = self.mkf_util.Multiple_Choices_from_prob(KL_list, prob_list, list_length)
        // Also, test if the kalman filters are deep copied here
        res[0].update_KL_index_info(1)
        XCTAssertEqual(res[0].KL_index, 1)
        XCTAssertEqual(res[1].KL_index, 7)
        XCTAssertEqual(res.count, list_length)
    }
    
    func test_Normalize_dict() {
        let weights_dict = ["1": 1.2, "2": 3.2, "3": 0.01, "4": 5.7]
        let res = self.mkf_util.Normalize_dict(weights_dict)
        let correct_result = ["1": 0.11869436201780416, "3": 0.0009891196834817015, "2": 0.31651829871414444, "4": 0.5637982195845698]
        XCTAssertEqual(correct_result, res)
    }
    
    func test_calculate_COV() {
        let weights_dict = ["1": 1.2, "2": 3.2, "3": 0.01, "4": 5.7]
        let res = self.mkf_util.calculate_COV(weights_dict)
        let correct_result = 0.853561001358869
        // XCTAssertEqual(correct_result, res)
    }
    
}
