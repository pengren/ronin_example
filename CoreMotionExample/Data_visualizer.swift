//
//  Data_visualizer.swift
//  CoreMotionExample
//
//  Created by renpeng on 2/11/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import Foundation
import Charts

class Data_visualizer{
    func generate_lines(_ input_seq: [Double], _ label_name: String, _ line_color: [UIColor]) -> LineChartDataSet{
            var chartEntry = [ChartDataEntry]()
            for i in 0..<input_seq.count {
                chartEntry.append(ChartDataEntry(x: Double(i)/25.0, y: input_seq[i]))
            }
            let line = LineChartDataSet(entries: chartEntry, label: label_name)
            line.drawCirclesEnabled = false
            line.drawValuesEnabled = false
            line.colors = line_color
            return line
        }
        
    func updateChart(_ origin_result: [Double], _ filtered_result: [Double], _ azimuth_result: [Double], _ chartView: LineChartView) {
            // Use this func to update the chart in realtime
            let data = LineChartData()
             
            let origin_pred_line = self.generate_lines(origin_result, "MKF results", [UIColor.red])
            let filtered_pred_line = self.generate_lines(filtered_result, "filtered predictions", [UIColor.purple])
            let azimuth_data_line = self.generate_lines(azimuth_result, "azimuth data", [UIColor.orange])
            
            data.addDataSet(origin_pred_line)
            data.addDataSet(filtered_pred_line)
            data.addDataSet(azimuth_data_line)
            
            chartView.rightAxis.drawGridLinesEnabled = false
            chartView.rightAxis.drawLabelsEnabled = false
    //      chartView.setVisibleXRangeMaximum(2)
    //      chartView.dragXEnabled = true
    //      chartView.doubleTapToZoomEnabled = false
    //      chartView.leftAxis.axisMinimum = -0.25
    //      chartView.leftAxis.axisMaximum = 1.25
            chartView.leftAxis.axisMinimum = -Double.pi
            chartView.leftAxis.axisMaximum = Double.pi
            chartView.data = data
            chartView.chartDescription?.text = "Turn detector Chart"
        }
    
    func update_magnetic_Chart(xMagField_seq: [Double], yMagField_seq: [Double], zMagField_seq: [Double], x_raw_seq: [Double], y_raw_seq: [Double], z_raw_seq: [Double], _ chartView: LineChartView) {
        // Use this function to visualize the magnetic data
        let data = LineChartData()
        let xMagField_seq_line = self.generate_lines(xMagField_seq, "xMagField", [UIColor.red])
        let yMagField_seq_line = self.generate_lines(yMagField_seq, "yMagField", [UIColor.orange])
        let zMagField_seq_line = self.generate_lines(zMagField_seq, "zMagField", [UIColor.blue])
        
        let x_raw_seq_line = self.generate_lines(x_raw_seq, "x_raw", [UIColor.green])
        let y_raw_seq_line = self.generate_lines(y_raw_seq, "y_raw", [UIColor.purple])
        let z_raw_seq_line = self.generate_lines(z_raw_seq, "z_raw", [UIColor.black])
        
        data.addDataSet(xMagField_seq_line)
        data.addDataSet(yMagField_seq_line)
        data.addDataSet(zMagField_seq_line)
        
        data.addDataSet(x_raw_seq_line)
        data.addDataSet(y_raw_seq_line)
        data.addDataSet(z_raw_seq_line)
        
        chartView.rightAxis.drawGridLinesEnabled = false
        chartView.rightAxis.drawLabelsEnabled = false
        
        chartView.data = data
        chartView.chartDescription?.text = "Magnetic Chart"
    }
    
    func single_line_chart(Line_seq_x: [Double], Line_seq_y: [Double], label: String) -> ScatterChartData{
        let data = ScatterChartData()
        
        var chartEntry = [ChartDataEntry]()
        for i in 0..<Line_seq_x.count {
            chartEntry.append(ChartDataEntry(x: Line_seq_x[i], y: Line_seq_y[i]))
        }
        
        chartEntry.sort(by: { $0.x < $1.x })
        let line = ScatterChartDataSet(entries: chartEntry, label: label)
        line.scatterShapeSize = 10.0
        
        line.highlightLineDashLengths = [5.0, 2.5]
        line.highlightColor = NSUIColor.black
        line.highlightLineWidth = 2.0
        line.drawValuesEnabled = false
        
        // line.drawCirclesEnabled = false
//        line.drawValuesEnabled = false
//        // line.colors = [UIColor.purple]
//        line.drawCirclesEnabled = true
//        line.circleRadius = 4
//        line.setCircleColor(.purple)
//        line.fillColor = .red
        data.addDataSet(line)
        return data
    }
}
