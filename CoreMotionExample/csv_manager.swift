//
//  csv_manager.swift
//  CoreMotionExample
//
//  Created by renpeng on 2/20/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import Foundation

public class csv_manager {
    var CSV_handler:CSV_oper
    init() {
        self.CSV_handler = CSV_oper()
    }
    // get document directory
    public func getDocumentDirectory () -> URL{
        if let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first{
            return url
        }
        else{
            fatalError("Unable to access document directory")
        }
    }
    
    public func return_target_directory(_ fileName:String) -> URL{
        return getDocumentDirectory().appendingPathComponent(fileName, isDirectory: false)
    }
    
    // Save any kind of codable objects
    public func save (fileName:String, output_data: [Double]){
        let url = getDocumentDirectory().appendingPathComponent(fileName, isDirectory: false)
        self.CSV_handler.Write_to_CSV(URL_path: url, output_data: output_data)
    }
    public func read(fileName:String, row: Int) -> [String]{
        let url = getDocumentDirectory().appendingPathComponent(fileName, isDirectory: false)
        return self.CSV_handler.Read_in_CSV(URL_path: url, row_num: row)
    }
}

