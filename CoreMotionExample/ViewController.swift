//
//  ViewController.swift
//  CoreMotionExample
//
//  Created by Peng Ren
//  Copyright © 2019 Peng Ren. All rights reserved.
//

import UIKit
import Charts

class ViewController: UIViewController{
    // The ChartView used to visualize the turn detection results & magnetic data
    @IBOutlet weak var chartView: ScatterChartView!
    @IBOutlet weak var magnetic_chartView: ScatterChartView!
    @IBOutlet weak var Refresh_rate: UILabel!
    override func viewDidLoad() {
		super.viewDidLoad()
        magnetic_chartView.xAxis.axisMinimum = -5
        magnetic_chartView.xAxis.axisMaximum = 5
        magnetic_chartView.leftAxis.axisMinimum = -5
        magnetic_chartView.leftAxis.axisMaximum = 5
        magnetic_chartView.rightAxis.axisMinimum = -5
        magnetic_chartView.rightAxis.axisMaximum = 5
        
//        let processor = process_pytorch_model()
//        processor.test_pytorch_model()
        // Start_process_pipline(self.chartView, self.magnetic_chartView)
        Start_pytorch_200(self.chartView, self.magnetic_chartView, self.Refresh_rate, self.view)
    }
}
