//
//  statistic_calculation.swift
//  CoreMotionExample
//
//  Created by renpeng on 9/18/19.
//  Copyright © 2019 Peng Ren. All rights reserved.
//

import Foundation

public class statistic_calculation {
    
    let pca_para = pca_raw_para()
    
    func obtain_l2_norm(_ input_array1: [Double], _ input_array2: [Double]) -> (Double){
        var array_diff_square = [Double]()
        for (array1, array2) in zip(input_array1, input_array2){
            array_diff_square.append((array1 - array2) * (array1 - array2))
        }
        let sum = array_diff_square.reduce(0, +)
        // Obtained diff, now get the l2 norm
        return sqrt(sum)
    }
    
    func split_target_data(_ input_data: [[[Double]]], _ select_dim: Int) -> ([Double]){
        // Simply get (32,) from (1, 32, 3) data
        var res = [Double]()
        
        for element_index in 0..<32{
            res.append(input_data[0][element_index][select_dim])
        }
        
        return res
        
    }
    
    func merge_3_seq_data(azimuth_combo: [Double], rotate_combo: [Double], userAcce_combo: [Double]) -> ([Double]){
        // merge three combo in a specific way
        var res = [Double]()
        for (azi, (rot, usr)) in zip(azimuth_combo, zip(rotate_combo, userAcce_combo)){
            res.append(azi)
            res.append(rot)
            res.append(usr)
        }
        return res
    }
    
    func merge_2_seq_data(azimuth_combo: [Double], rotate_combo: [Double]) -> ([Double]){
        // merge three combo in a specific way
        var res = [Double]()
        for (azi, rot) in zip(azimuth_combo, rotate_combo){
            res.append(azi)
            res.append(rot)
            // res.append(usr)
        }
        return res
    }
    
    func subtract_two_arrays(_ input_array1: [Double], _ input_array2: [Double]) -> ([Double]){
        var array_diff = [Double]()
        for (array1, array2) in zip(input_array1, input_array2){
            array_diff.append((array1 - array2))
        }
        return array_diff
    }
    
    func transpose_2d_array(_ input: [[Double]]) -> [[Double]] {
        let columns = input.count
        let rows = input.reduce(0) { max($0, $1.count) }
        
        var result: [[Double]] = []
        
        for row in 0 ..< rows {
            result.append([])
            for col in 0 ..< columns {
                if row < input[col].count {
                    result[row].append(input[col][row])
                } else {
                    result[row].append(0)
                }
            }
        }
        
        return result
    }
    
    func dotProduct(a: [Double], b: [Double]) -> Double {
        return zip(a, b).map(*).reduce(0, +)
    }
    
    func process_dot_product(subtract_res: [Double], pca_components: [[Double]]) -> [Double]{
        // produce the dot product manually
        var result: [Double] = []
        for single_components in pca_components{
            result.append(self.dotProduct(a: subtract_res, b: single_components))
        }
        return result
    }
    
    func calculate_pca_value(merged_combo: [Double]) -> ([Double]){
        // shrink the feature dim using pca
        let pca_mean = pca_para.pca_mean
        let pca_components = pca_para.pca_components
        let subtract_res = self.subtract_two_arrays(merged_combo, pca_mean)
        // let transposed_pac_components = self.transpose_2d_array(pca_components)
        let shrinked_res = self.process_dot_product(subtract_res: subtract_res, pca_components: pca_components)
        return shrinked_res
    }
    
    func online_scaler(feature_data: [Double]) -> ([Double]){
        // Used to preprocess the input features to step counter here, Untested
        let mean = Sigma.average(feature_data)!
        let std = Sigma.standardDeviationPopulation(feature_data)!
        var result: [Double] = []
        for single_data in feature_data{
            result.append((single_data - mean)/std)
        }
        return result
    }
}
