//
//  mkf_util.swift
//  CoreMotionExample
//
//  Created by renpeng on 11/29/19.
//  Copyright © 2019 Maxim Bilan. All rights reserved.
//

import AVFoundation
class MKF_util{
    public func Multiple_Choices_from_prob(_ KL_sample_list: [stand_alone_kalman_filter], _ candidate_prob_list: [Double], _ choice_number: Int)-> [stand_alone_kalman_filter]{
        var KL_list = [stand_alone_kalman_filter]()
        for _ in 0..<choice_number{
            KL_list.append(self.Choice_from_prob(KL_sample_list, candidate_prob_list) as! stand_alone_kalman_filter)
        }
        return KL_list
    }
    
    // TODO: Needs fully tests
    public func Choice_from_prob(_ orientation_candidate_list: [Any], _ candidate_prob_list: [Double])->Any{
        // 'Draw choice from list with prob in prob_list, can be repeated'
        let sum_of_prob = candidate_prob_list.reduce(0, +)
        let prob_list = candidate_prob_list.map { $0 / sum_of_prob }
        let random_index = self.randomNumber(probabilities: prob_list)
        return orientation_candidate_list[random_index]
    }
    
    public func randomNumber(probabilities: [Double]) -> Int {

        // Sum of all probabilities (so that we don't have to require that the sum is 1.0):
        let sum = probabilities.reduce(0, +)
        // Random number in the range 0.0 <= rnd < sum :
        let rnd = Double.random(in: 0.0 ..< sum)
        // Find the first interval of accumulated probabilities into which `rnd` falls:
        var accum = 0.0
        for (i, p) in probabilities.enumerated() {
            accum += p
            if rnd < accum {
                return i
            }
        }
        // This point might be reached due to floating point inaccuracies:
        return (probabilities.count - 1)
    }
    
    public func obtain_wrapped_distance(_ Mean: Double, _ New_y: Double)->Double{
        // Given the mean, value, return the wrapped distance between Mean and New_y
        // 'First, wrap the Mean, make sure it lives in [-pi, pi]'
        let wrapped_mean = self.wrap_azimuth_data(Mean)
        assert(New_y <= Double.pi && New_y >= -Double.pi, "your data should be wrapped!")
        // 'Second, get the wrapped distance between wrapped_mean and New_y'
        let real_distance = self.obtain_real_angle_change(wrapped_mean, New_y)
        return real_distance
    }
    
    public func custom_truncatingRemainder(_ input_val: Double, _ dividingBy: Double)->Double{
        var res = input_val.truncatingRemainder(dividingBy: dividingBy)
        if res < 0{
            res += dividingBy
        }
        assert(res>=0, "Your custom_truncatingRemainder not work..")
        return res
    }
    
    public func wrap_azimuth_data(_ phases: Double)->Double{
        let res = (self.custom_truncatingRemainder(-phases + Double.pi, 2.0 * Double.pi) - Double.pi) * -1.0
        assert(res <= Double.pi && res >= -Double.pi, "wrap azimuth out range")
        return res
    }
    
    public func obtain_real_angle_change(_ source: Double, _ target: Double)->Double{
        // 'get target - source true value'
        let angle_diff = target - source
        let true_angle_diff = self.custom_truncatingRemainder(angle_diff + Double.pi, 2*Double.pi) - Double.pi
        assert(true_angle_diff<=2*Double.pi, "result outof range")
        return true_angle_diff
    }
    
    public func normpdf(_ x: Double, _ mean: Double, _ std: Double)-> Double{
        let value = std * std
        let denom = sqrt(2*Double.pi*value)
        let num = exp(-(x-mean)*(x-mean)/(2*value))
        return num/denom
    }
        
    public func radius_to_string(_ radius: Double)->String{
        let float_val = 4 * radius/Double.pi
        // Here we acutally need to use the .towardZero instead of .down. Python use .towardZero
        var res = Int(float_val.rounded(.towardZero))*45
        if res == -180{
            res = 180
        }
        return String(res)
    }
    
    public func single_drift_update_limiter(_ update_dirft: Double)->Double{
        // 'First, obtain the sign'
        let sign_of_drift = self.custom_sign(update_dirft)
        // 'Then, get the update value'
        let limited_update_val = self.custom_sigmoid_func(abs(update_dirft))
        // 'Do not forget to fetch the sign'
        let result = sign_of_drift*limited_update_val
        return result
    }
    
    public func custom_sigmoid_func(_ X: Double)->Double{
        // TODO: add an equal sign here... needs further investigate
        assert(X >= 0.0, "the input should be a abs value")
        let a = 1.67
        let b = -72.0
        let k = 0.001
        return k / (1 + exp(a + b*X))
    }
    
    public func custom_sign(_ X: Double) -> Double{
        if X < 0{
            return -1.0
        }
        else if X > 0{
            return 1.0
        }
        else{
            return 0.0
        }
    }
    
    public func round_to_nearest_orientation(_ input_azimuth: Double) -> Double{
        // 2. 90 Change <<<
        // return (4 * input_azimuth/Double.pi).rounded() * Double.pi/4.0
        return (2 * input_azimuth/Double.pi).rounded() * Double.pi/2.0
    }
    
    public func Normalize_dict(_ weights_dict: Dictionary<String, Double>) -> Dictionary<String, Double>{
        var new_weights_dict = weights_dict
        let value_sum = weights_dict.values.reduce(0, +)
        for (key, value) in weights_dict{
            new_weights_dict[key] = value/value_sum
        }
        assert(abs(new_weights_dict.values.reduce(0, +) - 1.0) < 0.0001, "Seems the normalized dictionary value is wrong")
        return new_weights_dict
    }
    
    public func calculate_COV(_ weights_dict: Dictionary<String, Double>)->Double{
        let value = weights_dict.map { return $0.value }
        let mean = Sigma.average(value)!
        let stddev = Sigma.standardDeviationPopulation(value)!
        return stddev/mean
    }
}
