//
//  Data_Generation.swift
//  CoreMotionExample
//
//  Created by renpeng on 12/22/18.
//  Copyright © 2018 Peng Ren. All rights reserved.
//

import Foundation
import CoreMotion
public class Data_Generation {
    static func attitude_gen(deviceMotion:CMDeviceMotion) -> (Double, CMRotationMatrix) {
        var attitude: CMAttitude?
        attitude = deviceMotion.attitude
        // There might be problems here for the !
        let rotationMatrix = attitude!.rotationMatrix
        let Res = RotationMatrix_Extraction.extract_rotation_matrix(rotation_matrix: rotationMatrix)
        return (Res.yaw, rotationMatrix)
    }
    
    static func magnetic_field_gen(deviceMotion:CMDeviceMotion) -> (Double, Double, Double) {
        var magnetic_filed: CMCalibratedMagneticField
        magnetic_filed = deviceMotion.magneticField
        let xMagField=magnetic_filed.field.x
        let yMagField=magnetic_filed.field.y
        let zMagField=magnetic_filed.field.z
        return (xMagField, yMagField, zMagField)
    }
    
    static func magnetic_raw_gen(motionManager:CMMotionManager) -> (Double?, Double?, Double?) {
        let x_raw=motionManager.magnetometerData?.magneticField.x
        let y_raw=motionManager.magnetometerData?.magneticField.y
        let z_raw=motionManager.magnetometerData?.magneticField.z
        return (x_raw, y_raw, z_raw)
    }
    
    static func obtain_three_dim_magnitude(x: Double, y: Double, z: Double) -> (Double) {
        // deprecated
        return sqrt(x*x + y*y + z*z)
    }
}
