//
//  CSV_oper.swift
//  CoreMotionExample
//
//  Created by renpeng on 2/20/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import Foundation
import CSV

class CSV_oper {
    
    public func Read_in_CSV(URL_path: URL, row_num: Int) -> [String] {
        let stream = InputStream(fileAtPath: URL_path.path)
        let csv = try! CSVReader(stream: stream!)
        var res_array:[String] = []
        while let row = csv.next() {
            res_array.append(row[row_num])
        }
        return res_array
    }
    public func Write_to_CSV(URL_path: URL, output_data: [Double]) -> Void{
        guard let stream = OutputStream(toFileAtPath: URL_path.path, append: false) else { return }
        
        let csv = try! CSVWriter(stream: stream)
        
        for i in 0..<output_data.count {
            do {
                try csv.write(row: [String(output_data[i])])
            } catch {
                print("Something wrong here")
            }
        }
        csv.stream.close()
    }
}
