//
//  Merge_label.swift
//  CoreMotionExample
//
//  Created by renpeng on 12/2/19.
//  Copyright © 2019 Peng Ren. All rights reserved.
//

import AVFoundation
class Merge_label{
    // Result after merge happens
    var merged_result = [Double]()
    // Save the previous interval labels
    var last_interval_label = [Double]()
    // For the interval position counter
    var start_counter = 0
    let mkf_util = MKF_util()
    
    // Line 1. Create an instance of AVSpeechSynthesizer.
    var speechSynthesizer = AVSpeechSynthesizer()
    
    public func new_data_available(mkf_pred: Double, interval_label: Double){
        
        let last_mkf_pred = merged_result.last ?? 0.0
        let last_label = last_interval_label.last ?? 0.0
        
        if last_label == 0.0 && interval_label == 1.0{
            // enter the interval
            start_counter = self.merged_result.count
        }
        else if last_label == 1.0 && interval_label == 0.0{
            // end the interval
            // Modify the input values
            let end_counter = merged_result.count
            
            if self.merged_result[start_counter] != self.merged_result[end_counter-1]{
                let angle_diff = mkf_util.obtain_real_angle_change(merged_result[start_counter], merged_result[end_counter-1])
                // read out turn angles
                self.speak_out("\(self.mkf_util.radius_to_string(angle_diff)) degree turn")
            }
            
            let middle_counter = Int((start_counter + end_counter)/2)
            
            for index in start_counter..<middle_counter{
                self.merged_result[index] = self.merged_result[start_counter]
            }
            
            for index in middle_counter..<end_counter{
                self.merged_result[index] = self.merged_result[end_counter-1]
            }
        }
        else if interval_label == 0.0{
            if mkf_pred != last_mkf_pred{
                let angle_diff = mkf_util.obtain_real_angle_change(last_mkf_pred, mkf_pred)
                // read out turn angles
                self.speak_out("while walking, \(self.mkf_util.radius_to_string(angle_diff)) degree turn")
            }
        }
        
        merged_result.append(mkf_pred)
        last_interval_label.append(interval_label)
    }
    
    public func get_merged_result()->[Double]{
        return self.merged_result
    }
    
    public func speak_out(_ input: String) {

      // Line 2. Create an instance of AVSpeechUtterance and pass in a String to be spoken.

      let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: input)

      //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5

      speechUtterance.rate = AVSpeechUtteranceMaximumSpeechRate / 2.0

      // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.

      speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")

      // Line 5. Pass in the urrerance to the synthesizer to actually speak.

      self.speechSynthesizer.speak(speechUtterance)

    }
}
