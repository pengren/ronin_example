//
//  MKF.swift
//  CoreMotionExample
//
//  Created by renpeng on 11/30/19.
//  Copyright © 2019 Peng Ren. All rights reserved.
//

import AVFoundation
class MKF_class{
    let mkf_num: Int
    let COV_threshold: Double
    
    var weights_dict: Dictionary<String, Double>
    var KL_dict: Dictionary<String, stand_alone_kalman_filter>
    
    let mkf_util: MKF_util
    
    var expectation_trace: [Double]
    var faithful_expectation_trace: [Double]
    
    var start_expectation: Double
    var trace_holder: Double
    var trace_locked: Bool
    
    var process_resample: Bool
    var resample_cov_counter: Int
    
    let sample_delay: Int
    
    init(_ input_mkf_num: Int) {
        mkf_num = input_mkf_num
        COV_threshold = 0.30
        // *** 'initialize weights' ***
        weights_dict = [:]
        // 'Notice that the initial weights are averaged'
        let initial_weights = 1.0/Double(self.mkf_num)
        for KL_index in 0..<self.mkf_num{
            weights_dict["KL_\(KL_index)"] = initial_weights
        }
        // *** 'initialize each kalman filter' ***
        KL_dict = [:]
        for KL_index in 0..<self.mkf_num{
            KL_dict["KL_\(KL_index)"] = stand_alone_kalman_filter(KL_index)
        }
        // import util func
        mkf_util = MKF_util()
        
        // 'Saved for the final results'
        // 'locked expecation'
        expectation_trace = []
        // 'unlocked expectation'
        faithful_expectation_trace = []
        
        // 'Start result for the final results'
        start_expectation = 0.0
        
        // 'lock parameters, do not change your direction in the process of resampling'
        trace_holder = 0.0
        trace_locked = false
        
        // 'process resample flag, do not resample until the COV consist larger than a threshold'
        process_resample = false
        resample_cov_counter = 0
        
        sample_delay = 10
    }
    
    public func return_Expecation_trace() -> [Double]{
        return self.expectation_trace
    }
    
    public func new_data_available(_ New_y: Double, _ single_pred_label: Double)->Double{
        for KL_index in 0..<self.mkf_num{
            let KL_name = "KL_\(KL_index)"
            let update_weights = self.KL_dict[KL_name]!.single_kalman_filter_pipline(New_y, single_pred_label: single_pred_label)
            self.weights_dict[KL_name]! *= update_weights
        }
        // 'Normalize weights so that the sum is 1'
        self.weights_dict = self.mkf_util.Normalize_dict(self.weights_dict)
        self.remove_path_with_small_prob(remove_thres: 10e-12)
        self.resampling_rejuvenation()
        self.weights_dict = self.mkf_util.Normalize_dict(self.weights_dict)
        return self.return_orientation()
    }
    
    public func remove_path_with_small_prob(remove_thres: Double){
        var KL_sample_list = [stand_alone_kalman_filter]()
        var weights_sample_list = [Double]()
        
        // 'If no need to resample, just return'
        // TODO: test if this min func really works
        if self.weights_dict.values.min()! > remove_thres{
            return
        }
//        print(self.weights_dict)
//        print(self.weights_dict.values.min()!)
        for KL_index in 0..<self.mkf_num{
            let KL_name = "KL_\(KL_index)"
            if self.weights_dict[KL_name]! > remove_thres{
                KL_sample_list.append(self.KL_dict[KL_name]!)
                weights_sample_list.append(self.weights_dict[KL_name]!)
            }
        }
        assert(KL_sample_list.count > 0, "no weights larger than \(remove_thres)")
        var New_KL_list = [stand_alone_kalman_filter]()
        if KL_sample_list.count == 1{
            // 'only one element, then just repeat it multiple times'
            for _ in 0..<self.mkf_num{
                New_KL_list.append(KL_sample_list[0])
            }
        }
        else{
            New_KL_list = self.mkf_util.Multiple_Choices_from_prob(KL_sample_list, weights_sample_list, self.mkf_num)
        }
        // 'New_KL_list: Used to record the new resampled KL order'
        self.process_resample_trunk(New_KL_list)
    }
    
    public func resampling_rejuvenation(){
        // 'resample the KL filters when the weights has large COV'
        let COV_value = self.mkf_util.calculate_COV(self.weights_dict)
        if COV_value > self.COV_threshold && !self.process_resample{
            self.resample_cov_counter += 1
        }
        
        if COV_value < self.COV_threshold && self.resample_cov_counter != 0{
            self.resample_cov_counter = 0
            assert(self.process_resample == false, "logic error")
        }
        
        if self.resample_cov_counter >= self.sample_delay{
            self.resample_cov_counter = 0
            self.process_resample = true
        }
        
        if self.process_resample{
            self.process_resample = false
            self.for_loop_for_resample(5)
        }
    }
    
    public func for_loop_for_resample(_ max_iter: Int){
        // 'a for loop, do not exist until the resample process finished, or iter max_iter times'
        // 'resample until the COV is below a threshold here'
        for _ in 0..<max_iter{
            // Get current KL_values
            let KL_values = self.KL_dict.map { return $0.value }
            let weights_values = self.weights_dict.map { return $0.value }
            
            let New_KL_list = self.mkf_util.Multiple_Choices_from_prob(KL_values, weights_values, self.mkf_num)
            self.process_resample_trunk(New_KL_list)
            let after_resample_cov = self.mkf_util.calculate_COV(self.weights_dict)
            if after_resample_cov < self.COV_threshold{
                break
            }
        }
    }
    
    public func process_resample_trunk(_ New_KL_list: [stand_alone_kalman_filter]){
        // print("RESAMPLED!")
        let previous_weights = self.weights_dict
        // 'update previous dict'
        for KL_index in 0..<self.mkf_num{
            let KL_name = "KL_\(KL_index)"
            // 'Please, do not forget to update weights...'
            assert(previous_weights.keys.contains(New_KL_list[KL_index].KL_name) , "Your input weights_dict might not be right here")
            self.weights_dict[KL_name] = previous_weights[New_KL_list[KL_index].KL_name]
            // 'Deep copy the class instance'
            self.KL_dict[KL_name] = New_KL_list[KL_index]
            // 'Update index inside KL instance'
            self.KL_dict[KL_name]!.update_KL_index_info(KL_index)
        }
        // 'Well, also do not forget to normalize the new weights... It matters'
        self.weights_dict = self.mkf_util.Normalize_dict(self.weights_dict)
    }
    
    public func return_orientation()->Double{
        let expectation = self.obtain_orientation_expectation()
        // 'round operation'
        let round_expectation = self.mkf_util.round_to_nearest_orientation(expectation)
        self.faithful_expectation_trace.append(expectation)
        
        // 'Do not change direction while we are resampling...'
        if self.resample_cov_counter != 0 && !self.trace_locked{
            // 'Hold the trace, show old trace here'
            if self.faithful_expectation_trace.count > 1{
                self.trace_holder = 0.0
            }
            self.trace_locked = true
        }
        else if self.resample_cov_counter != 0 && self.trace_locked{
            // Do nothing
        }
        else{
            assert (self.process_resample == false, "logic error")
            assert (self.resample_cov_counter == 0, "logic error")
            self.trace_locked = false
            self.trace_holder = round_expectation
        }
        
        self.start_expectation += self.trace_holder
        // 'Wrap the expectation'
        self.start_expectation = self.mkf_util.wrap_azimuth_data(self.start_expectation)
        self.expectation_trace.append(self.start_expectation)
        // TODO: See if this is the right return value
        return self.start_expectation
    }
    
    public func obtain_orientation_expectation()->Double{
        // 'Use results from all the KL filters to calculate an expectation'
        var expectation_val = 0.0
        for KL_index in 0..<self.mkf_num{
            let KL_name = "KL_\(KL_index)"
            let past_expectation_value: Double
            past_expectation_value = self.expectation_trace.last ?? 0.0
            let absolute_diff = self.KL_dict[KL_name]!.obtain_orientation_change_from_expectation(past_expectation_value)
            // print("at \(KL_index), cal \(absolute_diff) * \(self.weights_dict[KL_name]!)")
            expectation_val += absolute_diff*self.weights_dict[KL_name]!
        }
        return expectation_val
    }
}
