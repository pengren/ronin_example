//
//  Coordinate_normalization.swift
//  CoreMotionExample
//
//  Created by renpeng on 2/18/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import Foundation
import CoreMotion
import Charts

class Coordinate_normalization{
    // Constant
    let gravity_constant: Double
    var start_chart_size: Double
    
    // Recorder
    var data_sink: [[Double]]
    var x_start: Double // x position start at 0
    var y_start: Double // y position start at 0
    var x_pos: [Double] // records the resulted position x
    var y_pos: [Double] // records the resulted position y
    var start: DispatchTime
    var timeInterval: Double
    
    // Declare of different classes
    let processor: process_pytorch_model!
    var chartView: ScatterChartView!
    var close_chartView: ScatterChartView!
    let chart_manager: Data_visualizer!
    var Refresh_rate: UILabel!
    let view: UIView!
    let csv_handler: csv_manager!
    
    // Normalize the coordinate & prepare the data to the RONIN model
    init(_ chartView: ScatterChartView, _ close_chartView: ScatterChartView,_ Refresh_rate: UILabel, _ view: UIView!) {
        // Constant
        self.gravity_constant = -9.81
        self.start_chart_size = 1.0
        
        // Recorder
        self.data_sink = [[Double]]()
        self.x_start = 0.0
        self.y_start = 0.0
        self.x_pos = [Double]()
        self.y_pos = [Double]()
        self.start = DispatchTime.now()
        self.timeInterval = 0.0
        
        // Declare of different classes
        self.processor = process_pytorch_model()
        self.chartView = chartView
        self.close_chartView = close_chartView
        self.chart_manager = Data_visualizer()
        self.Refresh_rate = Refresh_rate
        self.view = view
        self.csv_handler = csv_manager()
        
        // TMP buttton here, used to save csv
//        let button = UIButton()
//        button.frame = CGRect(x: self.view.frame.size.width - 60, y: 60, width: 50, height: 50)
//        button.backgroundColor = UIColor.red
//        button.setTitle("Name your Button ", for: .normal)
//        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
//        self.view.addSubview(button)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        // Debug purpose
        let align_count = min(self.x_pos.count, self.y_pos.count)
        self.csv_handler.save(fileName: "\(obtain_current_time())_x_path.csv", output_data: self.x_pos.suffix(align_count))
        self.csv_handler.save(fileName: "\(obtain_current_time())_y_path.csv", output_data: self.y_pos.suffix(align_count))
    }
    
    func obtain_current_time()->(String){
        // Debug purpose
        let dateFormatter : DateFormatter = DateFormatter()
        // dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.dateFormat = "dd_HH_mm_"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        print(dateString)
        return dateString
    }
    
    func process_data(accelerometerData: CMAccelerometerData, gyroData: CMGyroData, rotationMatrix: CMRotationMatrix){
        // extract different kinds of data here
        let gyroData_x = gyroData.rotationRate.x
        let gyroData_y = gyroData.rotationRate.y
        let gyroData_z = gyroData.rotationRate.z
        
        // Multiply by gravity
        let acceData_x = accelerometerData.acceleration.x * self.gravity_constant
        let acceData_y = accelerometerData.acceleration.y * self.gravity_constant
        let acceData_z = accelerometerData.acceleration.z * self.gravity_constant
        
        // Normalize the data above
        let gyroData_normalized = self.corrdinate_norm(rotationMatrix: rotationMatrix, x: gyroData_x, y: gyroData_y, z: gyroData_z)
        let acceData_normalized = self.corrdinate_norm(rotationMatrix: rotationMatrix, x: acceData_x, y: acceData_y, z: acceData_z)
        let combined_normalized_data = gyroData_normalized + acceData_normalized
        data_sink.append(combined_normalized_data)
        if data_sink.count >= 200 && data_sink.count % 10 == 0{
            // As we expected, this part runs 0.05 seconds
            let end = DispatchTime.now()
            let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
            self.timeInterval = Double(nanoTime) / 1_000_000_000
            print("Model running time: \(self.timeInterval) seconds")
            self.start = end
            
            //Perform some task and update UI immediately.
            DispatchQueue.global(qos: .userInitiated).async {
                let RONIN_output = self.Fire_model_RONIN()
                // The time here should be equal to the time interval above
                self.x_start += RONIN_output[0] * self.timeInterval
                self.y_start += RONIN_output[1] * self.timeInterval
                
                self.x_pos.append(self.x_start)
                self.y_pos.append(self.y_start)
                
                // Sample so that resulted in 50 samples
                let (x_resampled, y_resampled) = self.subsample_data(self.x_pos, self.y_pos, 50)
                let line_data = self.chart_manager.single_line_chart(Line_seq_x: x_resampled, Line_seq_y: y_resampled, label: "Path from RONIN")
                
//                var last_zero_x_pos = [Double]()
//                var last_zero_y_pos = [Double]()
//
//                for x_single in self.x_pos.suffix(100){
//                    last_zero_x_pos.append(x_single - self.x_pos.last!)
//                }
//                for y_single in self.y_pos.suffix(100){
//                    last_zero_y_pos.append(y_single - self.y_pos.last!)
//                }
//
//                let part_data = self.chart_manager.single_line_chart(Line_seq_x: last_zero_x_pos, Line_seq_y: last_zero_y_pos, label: "Zoom in path from RONIN")
                // print("[\(self.x_start), \(self.y_start)]")
                
                // Update the chart size if necessary
                if abs(self.x_start) > self.start_chart_size || abs(self.y_start) > self.start_chart_size{
                    self.start_chart_size *= 2.0
                }
                DispatchQueue.main.async {
                    self.chartView.data = line_data
                    self.chartView.highlightValue(x: x_resampled.last!, y: y_resampled.last!, dataSetIndex: 0, callDelegate: true)
                    if self.chartView.xAxis.axisMaximum != self.start_chart_size{
                        self.set_chartview_size(target_size:self.start_chart_size)
                    }
                    // Monitor the refresh rates here
                    self.Refresh_rate.text = "refresh rate: \(String(format: "%.5f", self.timeInterval))"

//                    self.close_chartView.data = part_data
//                    self.close_chartView.highlightValue(x: last_zero_x_pos.last!, y: last_zero_y_pos.last!, dataSetIndex: 0, callDelegate: true)
                }
                // print(RONIN_output)
            }
        }
    }
    
    func corrdinate_norm(rotationMatrix: CMRotationMatrix, x: Double, y: Double, z: Double)->[Double]{
        // Use rotationMatrix to normalize the coordinate here
        let row1 = rotationMatrix.m11 * x + rotationMatrix.m21 * y + rotationMatrix.m31 * z
        let row2 = rotationMatrix.m12 * x + rotationMatrix.m22 * y + rotationMatrix.m32 * z
        let row3 = rotationMatrix.m13 * x + rotationMatrix.m23 * y + rotationMatrix.m33 * z
        return [row1, row2, row3]
    }
    
    func Fire_model_RONIN()->[Double]{
        // Run the RONIN model here
        assert(self.data_sink.count >= 200, "You don't even have enough data here!")
        // Get last 200 newest data, this should not be a problem
        let input_data = Array(self.data_sink.suffix(200))
        assert(input_data[0].count == 6, "check your code")
        assert(input_data.count == 200, "check your code")
        return self.processor.RONIN_run(Input_val: input_data)
    }
    
    func set_chartview_size(target_size:Double){
        // Enlarge chartview size
        chartView.xAxis.axisMaximum = target_size
        chartView.xAxis.axisMinimum = -target_size
        chartView.leftAxis.axisMinimum = -target_size
        chartView.leftAxis.axisMaximum = target_size
        chartView.rightAxis.axisMinimum = -target_size
        chartView.rightAxis.axisMaximum = target_size
    }
    
    func subsample_data(_ x_data: [Double], _ y_data: [Double], _ sample_res_length: Int)->([Double], [Double]){
        // Sample data so that the results length near sample_res_length
        // First, define sample step
        if x_data.count <= sample_res_length || y_data.count <= sample_res_length{
            return (x_data, y_data)
        }
        
        let count_step = Int(x_data.count/sample_res_length)
        var x_res = [Double]()
        var y_res = [Double]()
        
        for i in stride(from: 0, to: x_data.count, by: count_step) {
            x_res.append(x_data[i])
        }
        for i in stride(from: 0, to: y_data.count, by: count_step) {
            y_res.append(y_data[i])
        }
        return (x_res, y_res)
    }
    
}
