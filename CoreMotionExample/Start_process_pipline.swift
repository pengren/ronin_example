//
//  Start_process_pipline.swift
//  CoreMotionExample
//
//  Created by renpeng on 2/11/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import AVFoundation
import CoreMotion
import UIKit
import Charts

class Start_process_pipline{
    // Constants
    let window_size: Int // Size of input to the RNN during training process, note that we do not train the model here
    let turn_prob_threshold: Double // If the class 1 prob is larger than this threshold, the result is 1. Otherwise, the result is 0
    let delay_threshold: Int // If a turn interval last less than this threshold, we ignore the turn interval
    let view_length: Int // The length of the data to be visualized
    
    // Results recorder
    var azimuth_data: [Double] // Array contains the wrapped data
    var Classifier_prob_recorder: [Double] // Array contains the class 1 probability
    var filtered_result_recorder: [Double] // Array contains the turn interval detection results
    var mkf_result: [Double] // Array contains the facing directions returned by MKF
    var raw_azimuth_start_point: Double // Make sure the azimuth always start at zero
    
    // Magnetic_results_recorder
    var xMagField_seq: [Double]
    var yMagField_seq: [Double]
    var zMagField_seq: [Double]
    
    var x_raw_seq: [Double]
    var y_raw_seq: [Double]
    var z_raw_seq: [Double]
    
    // Control variables
    var final_decision: Int // Used to count the turn interval length
    var final_decision_abnormal_counter: Int // Used to count the turn interval length
    var final_decision_normal_counter: Int // Used to count the turn interval length
    var during_turn: Bool // Used to control the ring before and after the turn
    var first_azimuth: Bool // Used to subtract the very first azimuth bais
    
    // Declare of different classes
    let motionManager: CMMotionManager!
    let chart_manager: Data_visualizer!
    let semaphore: DispatchSemaphore!
    var mkf_instance: MKF_class!
    var label_merger: Merge_label!
    var GRU_turn_detector: RNN_turn_detector!
    var chartView: LineChartView!
    var magnetic_chartView: LineChartView!
    var wrap_util: MKF_util!
    var timer: Timer!
    
    init(_ chartView: LineChartView, _ magnetic_chartView: LineChartView) {
        // Constants
        self.window_size = 50
        self.turn_prob_threshold = 0.42810735
        self.delay_threshold = 10
        self.view_length = 50
        
        // Results recorder
        self.azimuth_data = [Double]()
        self.Classifier_prob_recorder = [Double]()
        self.filtered_result_recorder = [Double]()
        self.mkf_result = [Double]()
        self.raw_azimuth_start_point = 0.0
        
        // Magnetic_results_recorder
        self.xMagField_seq = [Double]()
        self.yMagField_seq = [Double]()
        self.zMagField_seq = [Double]()
        
        self.x_raw_seq = [Double]()
        self.y_raw_seq = [Double]()
        self.z_raw_seq = [Double]()
        
        // Control variables
        self.final_decision = -1
        self.final_decision_abnormal_counter = 0
        self.final_decision_normal_counter = 0
        self.during_turn = false
        self.first_azimuth = true
        
        // Declare of different classes
        self.motionManager = CMMotionManager()
        self.chart_manager = Data_visualizer()
        self.semaphore = DispatchSemaphore(value: 0)
        self.mkf_instance = MKF_class(50) // We use 50 kalman filters in total
        self.label_merger = Merge_label()
        self.GRU_turn_detector = RNN_turn_detector(hidden_unit: 32)
        self.chartView = chartView
        self.magnetic_chartView = magnetic_chartView
        self.wrap_util = MKF_util()
        self.timer = Timer.scheduledTimer(timeInterval: 1/25, target: self, selector: #selector(self.update), userInfo: nil, repeats: true) // The timeInterval here is 1/25, in other words, 25 HZ
        
        // Initialize sensors, choose which sensor to open
        self.Select_open_sensors()
    }
    
    func Select_open_sensors(){
        // motionManager.startAccelerometerUpdates()
        // motionManager.startGyroUpdates()
//      motionManager.deviceMotionUpdateInterval = 1/25
//      motionManager.magnetometerUpdateInterval = 1/25
//      motionManager.showsDeviceMovementDisplay = true
        motionManager.startMagnetometerUpdates()
        // motionManager.startDeviceMotionUpdates()
        // If we set the axis as xMagneticNorthZVertical, then our azimuth data does not begin at zero!
        // I have used the record_first_raw_azimuth func to solve this problem.
        motionManager.startDeviceMotionUpdates(using: .xMagneticNorthZVertical)
    }
    
    @objc func update() {
        // This func run 25 times per second
        if let deviceMotion = motionManager.deviceMotion {
            let (raw_azimuth, rotationMatrix) = Data_Generation.attitude_gen(deviceMotion: deviceMotion)
            self.record_first_raw_azimuth(raw_azimuth)
            // Rewrap the biased_removal data
            let cur_azimuth = self.wrap_util.wrap_azimuth_data(raw_azimuth - self.raw_azimuth_start_point)
            // print(cur_azimuth)
            azimuth_data.append(cur_azimuth)
            
            let (xMagField, yMagField, zMagField) = Data_Generation.magnetic_field_gen(deviceMotion: deviceMotion)
            let (x_raw, y_raw, z_raw) = Data_Generation.magnetic_raw_gen(motionManager: motionManager)
            
            // Fill resulted data into arrays
            self.xMagField_seq.append(xMagField)
            self.yMagField_seq.append(yMagField)
            self.zMagField_seq.append(zMagField)
            
            // If not available, use 0 as values
            self.x_raw_seq.append(x_raw ?? 0)
            self.y_raw_seq.append(y_raw ?? 0)
            self.z_raw_seq.append(z_raw ?? 0)
            
            self.async_turn_detector(cur_azimuth)
            self.decision_counter()
            self.MKF_process()
            self.Playsound_logic()
            self.update_chart()
        }
    }
    
    func record_first_raw_azimuth(_ raw_azimuth: Double){
        // Used to record the first azimuth value. Used to make sure the azimuth data begins at zero
        if self.first_azimuth{
            first_azimuth = false
            self.raw_azimuth_start_point = raw_azimuth
        }
    }
    
    func async_turn_detector(_ cur_azimuth: Double){
        // Perform turn detector based on GRU given the wrapped azimuth data
        DispatchQueue.global().async {
            self.GRU_turn_detector.process_input_azimuth_data(azimuth_data: cur_azimuth){
                    (result) -> () in
                    // The following code will be executed when the model is finished
                    self.Classifier_prob_recorder.append(result)
                    if result > self.turn_prob_threshold{
                        self.final_decision = 1
                    }
                    else{
                        self.final_decision = 0
                    }
                    self.semaphore.signal()
                }
            self.semaphore.wait()
        }
    }
    
    func decision_counter(){
        // Count the interval prediction, works like a filter
        if self.final_decision == 1{
            self.final_decision_abnormal_counter += 1
            self.final_decision_normal_counter = 0
        }
        else if self.final_decision == 0{
            self.final_decision_normal_counter += 1
            self.final_decision_abnormal_counter = 0
        }
        else{
            // Sufficient data here
            self.final_decision = -1
        }
    }
    
    func MKF_process(){
        // MKF update & label merge
        if self.azimuth_data.count >= self.window_size/2{
            // Data enough here, use azimuth [-25] as input data to MKF
            let mkf_raw_res = self.mkf_instance.new_data_available(
                self.azimuth_data[self.azimuth_data.count-self.window_size/2], self.filtered_result_recorder.last ?? 0.0)
            mkf_result.append(mkf_raw_res)
            self.label_merger.new_data_available(mkf_pred: mkf_raw_res, interval_label: self.filtered_result_recorder.last ?? 0.0)
        }
        else{
            // We assume the facing direction never changes within the first 1 seconds
            mkf_result.append(0.0)
            self.label_merger.new_data_available(mkf_pred: 0.0, interval_label: self.filtered_result_recorder.last ?? 0.0)
        }
    }
    
    func Playsound_logic(){
        // Play sound when the turn begin/end
        if self.final_decision_abnormal_counter >= self.delay_threshold{
            if !self.during_turn{
                // set the filtered data back
                self.during_turn = true
                AudioServicesPlaySystemSound(1111)
                AudioServicesPlaySystemSound(4095)
            }
            self.filtered_result_recorder.append(Double(1.0))
        }
        else if self.final_decision_normal_counter >= self.delay_threshold{
            if self.during_turn{
                // set the filtered data back
                self.during_turn = false
                AudioServicesPlaySystemSound(1110)
                AudioServicesPlaySystemSound(4095)
            }
            self.filtered_result_recorder.append(Double(0.0))
        }
    }
    
    func update_chart(){
        // Update charts in main thread
        DispatchQueue.main.async {
            self.chart_manager.updateChart(self.label_merger.get_merged_result().suffix(self.view_length), self.filtered_result_recorder.suffix(self.view_length), self.azimuth_data.suffix(self.view_length), self.chartView)
            self.chart_manager.update_magnetic_Chart(xMagField_seq: self.xMagField_seq.suffix(self.view_length), yMagField_seq: self.yMagField_seq.suffix(self.view_length), zMagField_seq: self.zMagField_seq.suffix(self.view_length), x_raw_seq: self.x_raw_seq.suffix(self.view_length), y_raw_seq: self.y_raw_seq.suffix(self.view_length), z_raw_seq: self.z_raw_seq.suffix(self.view_length), self.magnetic_chartView)
        }
    }
}
