//
//  Start_pytorch_200.swift
//  CoreMotionExample
//
//  Created by renpeng on 2/18/20.
//  Copyright © 2020 Peng Ren. All rights reserved.
//

import AVFoundation
import CoreMotion
import UIKit
import Charts

class Start_pytorch_200{
    // Declare recorder
    var start: DispatchTime
    var recorder: Int // records the total sample numbers
    
    // Declare of different classes
    let motionManager: CMMotionManager!
    let chart_manager: Data_visualizer!
    var chartView: ScatterChartView!
    var close_chartView: ScatterChartView!
    var Refresh_rate: UILabel!
    let view: UIView!
    var normalizer: Coordinate_normalization!
    var timer: Timer!
    
    init(_ chartView: ScatterChartView,_ close_chartView: ScatterChartView,_ Refresh_rate: UILabel, _ view: UIView!) {
        // Declare recorder
        self.start = DispatchTime.now()
        self.recorder = 0
                
        // Declare of different classes
        self.motionManager = CMMotionManager()
        self.chart_manager = Data_visualizer()
        self.chartView = chartView
        self.close_chartView = close_chartView
        self.Refresh_rate = Refresh_rate
        self.view = view
        self.normalizer = Coordinate_normalization(self.chartView, self.close_chartView, self.Refresh_rate, self.view)
        self.timer = Timer.scheduledTimer(timeInterval: 1/200, target: self, selector: #selector(self.update), userInfo: nil, repeats: true) // The timeInterval here is 1/200, in other words, 200 HZ
        // Initialize sensors, choose which sensor to open
        self.Select_open_sensors()
    }
    
    func Select_open_sensors(){
        motionManager.startAccelerometerUpdates()
        motionManager.startGyroUpdates()
        motionManager.startDeviceMotionUpdates()
    }
    
    @objc func update() {
        // This func run 200 times per second
        if let deviceMotion = motionManager.deviceMotion,
            let acceData  = motionManager.accelerometerData,
            let gyroData = motionManager.gyroData {
            // Execute this part only if we get every kinds of data
            let (raw_azimuth, rotationMatrix) = Data_Generation.attitude_gen(deviceMotion: deviceMotion)
            self.normalizer.process_data(accelerometerData: acceData, gyroData: gyroData, rotationMatrix: rotationMatrix)
            
            // Generally, always make sure you got the right data rate here
            if self.recorder % 200 == 0{
                let end = DispatchTime.now()
                let nanoTime = end.uptimeNanoseconds - start.uptimeNanoseconds
                let timeInterval = Double(nanoTime) / 1_000_000_000
                print("Time: \(timeInterval) seconds")
                self.start = end
            }
            self.recorder += 1
        }
    }
}

