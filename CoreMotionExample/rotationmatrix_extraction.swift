//
//  RotationMatrix_extraction.swift
//  CoreMotionExample
//
//  Created by renpeng on 12/22/18.
//  Copyright © 2018 Peng Ren. All rights reserved.
//

import Foundation
import CoreMotion
public class RotationMatrix_Extraction {
    static func extract_rotation_matrix(rotation_matrix: CMRotationMatrix) -> (roll:Double,pitch:Double,yaw:Double){
        // We just do not transpose the matrix, so in the following formulas, as you can see,
        // m32->m23 and so on :P
        let roll = atan2(rotation_matrix.m23, rotation_matrix.m33)
        let pitch = atan2(-rotation_matrix.m13, sqrt(pow(rotation_matrix.m23, 2) + pow(rotation_matrix.m33, 2)))
        let yaw = atan2(rotation_matrix.m12, rotation_matrix.m11)
        
        return (roll, pitch, yaw)
    }
}
