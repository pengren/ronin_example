//
//  stand_alone_kalman_filter.swift
//  CoreMotionExample
//
//  Created by renpeng on 11/29/19.
//  Copyright © 2019 Peng Ren. All rights reserved.
//

import AVFoundation
struct stand_alone_kalman_filter{
    // A simple stand alone KL filter implementation, for mkf usage
    // index of this kl filter
    var KL_index: Int
    var KL_name: String
    
    // mkf_parameters_initialization
    let transition_matrix: Dictionary<String, Double>
    let turn_transition_matrix: Dictionary<String, Double>
    var orientation_candidate_list: [Double]
    var orientation_recorder: [Double]
    
    var X_trace:[Double]
    var V_trace:[Double]
    
    var candidate_prob_list:[Double]
    var drift_change_tracker:[Double]
    var drift_fix_line:[Double]
    var update_trace:[Double]

    var SVM_pred_label_seq:[Double]
    var skip_KF_update:Bool
    
    // KL_parameters_initialization
    var X:simd_double2
    let F:simd_double2x2
    let H:simd_double2
    var P:simd_double2x2
    let sigma_w:Double
    let sigma_v:Double
    let Q:simd_double2x2
    let R:Double
    
    let most_recent_angle_change:Double
    let mkf_util: MKF_util
    
    // initialize parameters related to mkf
    init(_ KL_index: Int) {
        // import util func
        self.mkf_util = MKF_util()
        // basic parameters
        self.KL_index = KL_index
        self.KL_name = "KL_\(self.KL_index)"
        
        // *** mkf_parameters_initialization ***
        // 1. 90 Change <<<
        // let tuner_prob = 0.000075/64.0
        // let during_turn_prob = 0.0147/5.0
        
//        let tuner_prob = 0.000075/64.0
//        let during_turn_prob = 0.0147/7.0
        
        let tuner_prob = 0.000075/64.0
        let during_turn_prob = 0.0147/5.0
        
        self.transition_matrix = [
            "0": 1 - tuner_prob*2,
            "45": 0.0,
            "90": tuner_prob,
            "135": 0.0,
            "180": 0.0,
            "-45": 0.0,
            "-90": tuner_prob,
            "-135": 0.0
        ]
        
        self.turn_transition_matrix = [
            "0": 1 - during_turn_prob*2,
            "45": 0.0,
            "90": during_turn_prob,
            "135": 0.0,
            "180": 0.0,
            "-45": 0.0,
            "-90": during_turn_prob,
            "-135": 0.0
        ]
        
        // self.orientation_candidate_list = [0.0, Double.pi/4, -Double.pi/4, Double.pi/2, -Double.pi/2, 3*Double.pi/4, -3*Double.pi/4, Double.pi]
        
        self.orientation_candidate_list = [0.0, Double.pi/2, -Double.pi/2, Double.pi]
        
        // 'the list that records the past orientation'
        // 'First orienation is assumed to be 0'
        self.orientation_recorder = [0.0]
        
        self.X_trace = []
        self.V_trace = []
        self.candidate_prob_list = []
        self.drift_change_tracker = []
        self.drift_fix_line = []
        self.update_trace = []
        // 'record previous input SVM interval sequence'
        self.SVM_pred_label_seq = []
        self.skip_KF_update = false
        
        // *** KL_parameters_initialization ***
        // Drift begins at zero
        self.X = simd_double2(0.0, 1.0)
        
        // the state-transition model
        self.F = simd_double2x2(rows: [
            simd_double2(1.0, 0.0),
            simd_double2(0.0, 1.0)
        ])
       
        // the observation matrix use current orientation
        self.H = simd_double2(1.0, 0.0)
        // 'error covariance'
        self.P = simd_double2x2(rows: [
            simd_double2(1.0, 0.0),
            simd_double2(0.0, 0.0)
        ])
        // Stddev of process noise, needs tune
        self.sigma_w = 0.004
        // Stddev of measurement noise, needs tune
        self.sigma_v = 0.25
        self.Q = simd_double2x2(rows: [
            simd_double2(self.sigma_w, 0.0),
            simd_double2(0.0, 0.0)
        ])
        self.R = self.sigma_v*self.sigma_v
        self.most_recent_angle_change = 0.0
    }
    
    mutating func single_kalman_filter_pipline(_ New_y:Double, single_pred_label: Double) -> Double{
        // The whole pipleline of the single kalman filter
        // single_pred_label: label that denotes whether we have a turn or not: 0 is not, 1 is yes
        self.SVM_pred_label_seq.append(single_pred_label)
        if single_pred_label == 1.0 && self.SVM_pred_label_seq.count>=2{
            // 'Skip the case where first element SVM pred label is 1'
            self.skip_KF_update = true
        }
        else{
            self.skip_KF_update = false
        }
        // 'begin the new update process', is this the so-called deepcopy?
        let X = self.X
        let P = self.P
        // '1. generate new orienation according to New_y (y_{t+1}) and current kalman filter'
        let (new_orientation, candidate_prob_list) = self.generate_new_orientation(
            New_y, X, P, previous_orientation: self.orientation_recorder.last!, with_turn_transition_matrix: self.skip_KF_update)
        self.candidate_prob_list = candidate_prob_list
        
        // TODO: >> Check the new_orientation value
        self.orientation_recorder.append(new_orientation)
        let X_update_copy = self.X
        let P_update_copy = self.P
        // '2. Update the kl filter using the new_orientation generated'
        // TODO: >> Check the new_orientation value
        (self.X, self.P) = self.KL_update(X_update_copy, P_update_copy, New_y, new_orientation)
        // TODO: >> Check the X value here
        self.X_trace.append(self.X[0])
        // '3. return the sum of the candidate probability of all the classes'
        self.update_trace.append(self.candidate_prob_list.reduce(0, +))
        return self.candidate_prob_list.reduce(0, +)
    }
    
    func generate_new_orientation(_ New_y: Double, _ X: simd_double2, _ P: simd_double2x2, previous_orientation: Double, with_turn_transition_matrix: Bool) ->(Double, [Double]){
        // 'First, calculate probability of each orientation candidate'
        let candidate_prob_list = self.orientation_candidate_prob_cal(New_y, X, P, previous_orientation:previous_orientation, with_turn_transition_matrix:with_turn_transition_matrix)
        // 'Second, sample using the probability of this list. The next orientation is just the result'
        return (self.mkf_util.Choice_from_prob(self.orientation_candidate_list, candidate_prob_list) as! Double, candidate_prob_list)
    }
    
    func orientation_candidate_prob_cal(_ New_y: Double, _ X: simd_double2, _ P: simd_double2x2, previous_orientation: Double, with_turn_transition_matrix: Bool) -> [Double]{
        // 'calculate the orientation probability, The list of prob for all the orientation candidates'
        var candidate_prob_list = [Double]()
        for orientation_candidate in self.orientation_candidate_list{
            let first_prob = self.single_orientation_prob_cal(orientation_candidate, New_y, X, P)
            let second_prob = self.decide_transition_prob(previous_orientation, orientation_candidate, with_turn_transition_matrix)
            candidate_prob_list.append(first_prob * second_prob)
        }
        assert(candidate_prob_list.count>0, "something wrong here, your orientation list might be empty!")
        return candidate_prob_list
    }
    
    func single_orientation_prob_cal(_ orientation_candidate: Double, _ New_y: Double, _ X: simd_double2, _ P: simd_double2x2)-> Double{
        // 'calculate the probability of generating New_y if current orientation is orientation_candidate'
        var custom_H = self.H
        custom_H[1] = orientation_candidate
        // TODO: Check the value of X_
        let X_ = simd_mul(self.F, X)
        // TODO: Check the value of Mean
        let Mean = simd_reduce_add(custom_H*X_)
        // 'Get variance'
        // TODO: Check the value of P_
        let P_ = simd_add(simd_mul(self.F, simd_mul(P, self.F.transpose)), self.Q)
        // TODO: WARNING: This part should be fully checked!
        let Variance = simd_reduce_add(custom_H * P_ * custom_H) + self.R
        let stddev = sqrt(Variance)
        // 'Obtain the real distance, but notice that this is the wrapped one'
        let wrapped_distance = abs(self.mkf_util.obtain_wrapped_distance(Mean, New_y))
        let pdf_val = self.mkf_util.normpdf(wrapped_distance, 0, stddev)
        return pdf_val
    }
    
    func decide_transition_prob(_ previous_orientation: Double, _ orientation_candidate: Double, _ with_turn_transition_matrix: Bool)->Double{
        // 'Simply get the corresponding transition prob'
        let orientation_change = self.mkf_util.obtain_real_angle_change(previous_orientation, orientation_candidate)
        let orientation_change_angle = self.mkf_util.radius_to_string(orientation_change)
        
        assert(self.transition_matrix.keys.contains(orientation_change_angle), "orientation_change_angle not in transition_matrix")
        assert(self.turn_transition_matrix.keys.contains(orientation_change_angle), "orientation_change_angle not in turn_transition_matrix")
        if !with_turn_transition_matrix{
            return self.transition_matrix[orientation_change_angle]!
        }
        else{
            return self.turn_transition_matrix[orientation_change_angle]!
        }
    }
    
    func KL_update(_ X: simd_double2, _ P: simd_double2x2, _ New_y: Double, _ orientation: Double)->(simd_double2, simd_double2x2){
        var mutable_X = X
        var mutable_P = P
        
        // 'Update the KL model with the guessed orientation'
        var custom_H = self.H
        custom_H[1] = orientation
        // 'Make sure the np.matmul(custom_H, X_) stay within the [-pi, pi]
        if orientation == Double.pi{
            let test_case = simd_reduce_add(custom_H*simd_mul(self.F, X))
            if test_case > Double.pi || test_case < -Double.pi{
                custom_H[1] = -Double.pi
            }
        }
        // "Update of the error covariance, notice the sigma_w here is actually the variance"
        // TODO: Check value of P_new, expecially P[0][0]
        let P_new = 1.0 / ((1.0 / (self.sigma_v * self.sigma_v)) + (1.0 / (P[0][0] + self.sigma_w)))
        let wrapped_y = self.mkf_util.obtain_wrapped_distance(simd_reduce_add(custom_H*X), New_y)
        let calculated_update_val = (P_new / (self.sigma_v * self.sigma_v)) * (wrapped_y)
        let update_limiter = self.mkf_util.single_drift_update_limiter(calculated_update_val)
        // TODO: Check value of mutable_X,mutable_P
        mutable_X[0] = mutable_X[0] + update_limiter
        mutable_P[0][0] = P_new
        return (mutable_X, mutable_P)
    }
    
    mutating func update_KL_index_info(_ new_index: Int){
        self.KL_index = new_index
        self.KL_name = "KL_\(self.KL_index)"
    }
    
    func obtain_current_orientation_state()->Double{
        // 'current orientation'
        assert(self.orientation_recorder.count > 1, "wrong time to call this function")
        return self.orientation_recorder[self.orientation_recorder.count-2]
    }
    
    func obtain_orientation_change_from_expectation(_ previous_expectation: Double)->Double{
        let current_orientation = self.obtain_current_orientation_state()
        // 'From previous expectation to current orientation, how much do the model need to change?'
        let change_orientation = self.mkf_util.obtain_real_angle_change(previous_expectation, current_orientation)
        return change_orientation
    }
    
    func return_orientation_trace()->[Double]{
        var res = [Double]()
        for index in 1..<self.orientation_recorder.count{
            res.append(self.mkf_util.round_to_nearest_orientation(self.orientation_recorder[index]))
        }
        return res
    }
}
